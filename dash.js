/**
 * dash.js
 *
 * @license 2014, MIT
 * @author Jan Biasi (@janbiasi)
 *
 * Review the Project under
 * https://github.com/janbiasi/dashj.s
 */


/** Used as a safe reference for `undefined` in pre ES5 environments */
var undefined;

/** Used as the semantic version number */
var version = '0.0.1';

/** Author definition */
var author = "Jan Biasi";

/** Base dash application dependencie */
var dash = {
    version: version,
    author: author,
    errors: [],
    templateSettings: {
        evaluate: /\{\{([\s\S]+?)\}\}/g,
        interpolate: /\{\{=([\s\S]+?)\}\}/g,
        encode: /\{\{!([\s\S]+?)\}\}/g,
        use: /\{\{#([\s\S]+?)\}\}/g,
        define: /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
        conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
        iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
        varname: 'it',
        strip: true,
        append: true,
        selfcontained: false
    }
} || dash;

/**
 * Get a unique Timestamp
 * @return {string}
 */
var timestamp = function() {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    var curr_hour = d.getHours();
    var curr_min = d.getMinutes();
    var curr_sec = d.getSeconds();
    return curr_date.toString() + "-" + curr_month.toString() + "-" + curr_year.toString() + " " + curr_hour.toString() + ":" + curr_min.toString() + ":" + curr_sec.toString();

}

/**
 * Alias for the 'console.log()' function provided by the browser
 * @param  {string} txt
 * @param  {object} args
 * @return {void}
 */
var log = function(txt, args) {
    return console.log.apply(console, arguments);
};

/**
 * Add error to the dash application
 * @param  {string} str
 * @return {void}
 */
dash.error = function(str) {
    dash.errors.push(timestamp() + ":  " + str);
    console.error(str);
}

/**
 * Update the log.txt in the webroot
 * @param  {object} data 	Optional Data
 * @return {string}			Timestamp of creation
 */
dash.createLog = function(data) {
    data = typeof data !== 'undefined' ? data : dash.errors;
    $.ajax({
        url: "api/log.php",
        type: "POST",
        data: {
            errors: dash.stringify(dash.errors)
        },
        success: function(msg) {
            log("Ajax response: " + msg);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            dash.error("Ajax error: " + textStatus + " -> " + errorThrown);
        }
    });
}

/**
 * Serialize an object to a string
 * @param  {var} obj
 * @return {string}     	Serialized object/variable
 */
dash.stringify = function(obj) {
    try {
        return JSON.stringify(obj);
    } catch (e) {
        dash.error("Couldn't transform to string: " + e);
        return false;
    }
}

dash.getMatches = function(value) {
    var result, matches = [];
    while (result = dash.templateSettings.evaluate.exec(value)) {
        try {
            matches.push(result[1].split(".")[1]);
        } catch (e) {
            log("An Array splitting Error occured: " + e);
        }
    }
    return matches;
}

dash.render = function(obj) {

}
