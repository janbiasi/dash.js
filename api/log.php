<?php
	// Get configuration
	require_once("../configuration.php");

	// Get encoded data from client and generate date
	$err = json_decode($_POST["errors"]);
	$now = date("Y-m-d H:i:s");

	if(!empty($err)) {
		$file = LOGFILES . "error_log.txt";

		// Get content and fill up with array data
		$current = file_get_contents($file);
		$current .= "# Generated log at " + $now + "\n";

		foreach($err as $entry) {
			$current .= $entry . "\n";
		}

		file_put_contents($file, $current);
	}

	return ("Log created @ " . $now);
	